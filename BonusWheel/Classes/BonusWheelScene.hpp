//
//  BonusWheelScene.hpp
//  BonusWheel
//
//  Created by Jun Cho on 4/14/22.
//

#ifndef BonusWheelScene_hpp
#define BonusWheelScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"


enum PrizeType
{
    life30,
    brush3,
    gem35,
    hammer3,
    coin750,
    brush1,
    gem75,
    hammer1
};

struct Prize
{
public:
    std::string name;
    PrizeType type;
    cocos2d::Node* node;
    float dropWeight;
    
    Prize() { name = ""; dropWeight = 0; }
    
    Prize(std::string _name, PrizeType _type, cocos2d::Node* _node, float _dropWeight)
    {
        name = _name;
        type = _type;
        node = _node;
        dropWeight = _dropWeight;
    };
    
    ~Prize() { delete node; };
    
    friend void swap(Prize& first, Prize& second)
    {
        using std::swap;
        swap (first.name, second.name);
        swap (first.type, second.type);
        swap (first.node, second.node);
        swap (first.dropWeight, second.dropWeight);
    }
    
    Prize& operator=(Prize *prize)
    {
        swap(*this, *prize);
        return *this;
    };
};

class BonusWheelScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(BonusWheelScene);
    // Manages the click event for the play and claim button
    void touchEvent( Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
private:
    Prize* prizeList;
    int prizeCount;
    float currentSector;
    float dropWeightSum;
    bool prizeSelected;
    cocos2d::Label* playButtonLabel;
    cocos2d::ui::Button* playButton;
    Node* selectedPrize;
    Node* wheelSections;
    Node* wheelBorder;

    // Helper function to create a new sprite in the scene
    cocos2d::Sprite* createSprite(std::string path, Node* parent, cocos2d::Vec2 normalizedPosition = cocos2d::Vec2::ZERO, cocos2d::Vec2 scale = cocos2d::Vec2::ONE);
    
    // Helper function to create a new label in the scene
    cocos2d::Label* createLabel(std::string text, Node* parent, int size = 60, cocos2d::Vec2 normalizedPosition = cocos2d::Vec2(0.5, 0));
    
    // Helper function to create a new button in the scene
    cocos2d::ui::Button* createButton(std::string imagePath, Node* parent, cocos2d::Vec2 normalizedPosition = cocos2d::Vec2::ZERO, cocos2d::Vec2 scale = cocos2d::Vec2::ONE);

    // Create prize images with labels on the prize wheel. Prize information is stored in here for now but can make readable from spreadsheet.
    void InitializePrizeList(Node* parent);
    
    // Create a prize image based on prize type
    Node* createPrizeImage(PrizeType type, Node* parent);
    
    // Moves the created prize images into position depending on the number of different prizes
    void setPrizesOnWheel();
    
    // Selects a prize based on the drop weights
    int selectPrize();
    
    // Emulates a set number of spins, then outputs the results to the console
    void testPrizeRNG(int numSpins);
    
    // Spins the wheel twice then goes to the sector
    void spinWheelAnimation(int sector);
    
    // Instantiate a copy of the selected prize and play animation
    void showPrize(int sector);
    
    // Reset the scene so the play button can be clicked again
    void resetWheel();
};

#endif /* BonusWheelScene_hpp */
