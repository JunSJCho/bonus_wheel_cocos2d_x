//
//  BonusWheelScene.cpp
//  BonusWheel
//
//  Created by Jun Cho on 4/14/22.
//

#include "BonusWheelScene.hpp"
#include <cmath>

USING_NS_CC;
using namespace std;

Scene* BonusWheelScene::createScene()
{
    auto scene = Scene::create();
    auto layer = BonusWheelScene::create();
    scene->addChild(layer);
    return scene;
}

bool BonusWheelScene::init()
{
    if (!Layer::init())
        return false;

    wheelSections = createSprite("Assets/wheel_sections_8.png", this, Vec2(0.5, 0.5));
    wheelBorder = createSprite("Assets/wheel_border.png", this, Vec2(0.5, 0.5));
    createSprite("Assets/wheel_arrow.png", wheelBorder, Vec2(0.5, 0.95), Vec2(1, 1.15));
    InitializePrizeList(wheelSections);
    setPrizesOnWheel();
    playButton = createButton("Assets/spin_button.png", this, Vec2(0.5, 0.18), Vec2(0.8, 0.8));
    testPrizeRNG(1000);
    return true;
}

// Create a new sprite in the scene
Sprite* BonusWheelScene::createSprite(std::string path, Node* parent, cocos2d::Vec2 normalizedPosition, cocos2d::Vec2 scale)
{
    auto sprite = Sprite::create(path);
    parent->addChild(sprite);
    sprite->setPositionNormalized(normalizedPosition);
    sprite->setScale(scale.x, scale.y);
    return sprite;
}

// Create a new label in the scene
Label* BonusWheelScene::createLabel(std::string text, Node* parent, int size, cocos2d::Vec2 normalizedPosition)
{
    auto label = Label::createWithSystemFont(text, "Arial", size);
    label->enableBold();
    label->enableOutline(Color4B::BLACK, 4);
    parent->addChild(label);
    label->setNormalizedPosition(normalizedPosition);
    return label;
}

// Creates the play/claim button
ui::Button* BonusWheelScene::createButton(std::string imagePath, Node* parent, cocos2d::Vec2 normalizedPosition, cocos2d::Vec2 scale)
{
    auto button = ui::Button::create(imagePath);
    parent->addChild(button);
    button->setPositionNormalized(normalizedPosition);
    button->setScale(scale.x, scale.y);
    playButtonLabel = createLabel("Play On", button, 108, Vec2(0.5, 0.55));
    button->addTouchEventListener(CC_CALLBACK_2(BonusWheelScene::touchEvent, this));
    prizeSelected = false;
    return button;
}

// Create prize images with labels on the prize wheel
void BonusWheelScene::InitializePrizeList(Node* parent)
{
    prizeCount = 8;
    currentSector = 7.5;
    prizeList = new Prize[prizeCount]();
    
    prizeList[0] = new Prize("Life 30 min", life30, createPrizeImage(life30, parent), 20);
    prizeList[1] = new Prize("Brush 3X", brush3, createPrizeImage(brush3, parent), 10);
    prizeList[2] = new Prize("Gems 35", gem35, createPrizeImage(gem35, parent), 10);
    prizeList[3] = new Prize("Hammer 3X", hammer3, createPrizeImage(hammer3, parent), 10);
    prizeList[4] = new Prize("Coins 750", coin750, createPrizeImage(coin750, parent), 5);
    prizeList[5] = new Prize("Brush 1X", brush1, createPrizeImage(brush1, parent), 20);
    prizeList[6] = new Prize("Gems 75", gem75, createPrizeImage(gem75, parent), 5);
    prizeList[7] = new Prize("Hammer 1X", hammer1, createPrizeImage(hammer1, parent), 20);
    dropWeightSum = 0;
    for (int i = 0; i < prizeCount; i++)
        dropWeightSum += prizeList[i].dropWeight;
}

// Create a prize image based on prize type
Node* BonusWheelScene::createPrizeImage(PrizeType type, Node* parent)
{
    switch (type)
    {
        case life30:
        {
            auto sprite = createSprite("Assets/heart.png", parent, Vec2::ZERO, Vec2(1.1, 1));
            createLabel("\u221E", sprite, 108, Vec2(0.5, 0.6));
            createLabel("30", sprite, 60, Vec2(0.7, 0.3));
            createLabel("min", sprite, 32, Vec2(1, 0));
            return sprite;
        }
        case brush3:
        {
            auto sprite = createSprite("Assets/brush.png", parent, Vec2::ZERO, Vec2(0.75, 0.75));
            createLabel("x3", sprite, 80);
            return sprite;
        }
        case gem35:
        {
            auto sprite = createSprite("Assets/gem.png", parent);
            createLabel("x35", sprite);
            return sprite;
        }
        case hammer3:
        {
            auto sprite = createSprite("Assets/hammer.png", parent);
            createLabel("x3", sprite);
            return sprite;
        }
        case coin750:
        {
            auto sprite = createSprite("Assets/coin.png", parent);
            createLabel("x750", sprite);
            return sprite;
        }
        case brush1:
        {
            auto sprite = createSprite("Assets/brush.png", parent, Vec2::ZERO, Vec2(0.8, 0.8));
            createLabel("x1", sprite, 75);
            return sprite;
        }
        case gem75:
        {
            auto sprite = createSprite("Assets/gem.png", parent);
            createLabel("x75", sprite);
            return sprite;
        }
        case hammer1:
        {
            auto sprite = createSprite("Assets/hammer.png", parent);
            createLabel("x1", sprite);
            return sprite;
        }
    }
}

// Move the created prize images into position depending on the number of different prizes
void BonusWheelScene::setPrizesOnWheel()
{
    for (int i = 0; i < prizeCount; i++)
    {
        prizeList[i].node->setRotation(360 / prizeCount * i + (180 / prizeCount));
        float angle = (-4 * i - 2 + prizeCount) * M_PI / (2 * prizeCount);
        Vec2 position = Vec2(cos(angle) * 0.35 + 0.5, sin(angle) * 0.35 + 0.5);
        prizeList[i].node->setNormalizedPosition(position);
    }
}

// Selects a prize based on the drop weights
int BonusWheelScene::selectPrize()
{
    float selection = random((float)0, dropWeightSum);
    for (int i = 0; i < prizeCount; i++)
    {
        if (selection <= prizeList[i].dropWeight)
            return i;
        selection -= prizeList[i].dropWeight;
    }
    return prizeCount - 1;
}

// Emulates a set number of spins, then outputs the results to the console
void BonusWheelScene::testPrizeRNG(int numSpins)
{
    int* pickedPrizeCounts = new int[prizeCount]();
    for (int i = 0; i < numSpins; i++)
        pickedPrizeCounts[selectPrize()]++;
    printf("\nOut of %i spins, the following prizes were picked this many times:\n", numSpins);
    for (int i = 0; i < prizeCount; i++)
    {
        printf(prizeList[i].name.c_str());
        printf(": %i\n", pickedPrizeCounts[i]);
    }
    printf("\n");
}

// Manages the click event for the play/claim button
void BonusWheelScene::touchEvent(Ref *sender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED)
    {
        if (!prizeSelected)
        {
            spinWheelAnimation(selectPrize());
            prizeSelected = true;
            playButton->setVisible(false);
            playButtonLabel->setString("Claim");
        }
        else
        {
            prizeSelected = false;
            resetWheel();
            playButtonLabel->setString("Play On");
        }
    }
}

// Spins the wheel twice then goes to the target sector
void BonusWheelScene::spinWheelAnimation(int sector)
{
    float amountToRotate = -((float)sector - currentSector + 0.5) / prizeCount;
    if (amountToRotate > 0)
        amountToRotate -= 1;
    amountToRotate -= 2;
    auto easeBack = EaseCubicActionInOut::create(RotateBy::create(0.5, 0.5 * 360 / prizeCount));
    auto easeForward = EaseCubicActionInOut::create(RotateBy::create(3, amountToRotate * 360));
    auto callbackFinished = CallFunc::create([this, sector](){ showPrize(sector); });
    wheelSections->runAction(Sequence::create(easeBack, easeForward, callbackFinished, NULL));
    currentSector = (float)sector;
}

// Instantiate a copy of the selected prize and play animation
void BonusWheelScene::showPrize(int sector)
{
    wheelSections->setVisible(false);
    wheelBorder->setVisible(false);
    playButton->setVisible(true);
    selectedPrize = createPrizeImage(prizeList[sector].type, this);
    selectedPrize->setNormalizedPosition(Vec2(0.5, 0.7));
    auto movePrize = EaseQuadraticActionIn::create(MoveBy::create(1, Vec2(0, -384)));
    auto callbackFinished = CallFunc::create([this](){ playButton->setVisible(true); });
    selectedPrize->runAction(ScaleBy::create(1, 2.0f));
    selectedPrize->runAction(Sequence::create(movePrize, callbackFinished, NULL));
}

// Reset the scene so the play button can be clicked again
void BonusWheelScene::resetWheel()
{
    selectedPrize->removeFromParent();
    wheelSections->setVisible(true);
    wheelBorder->setVisible(true);
}
