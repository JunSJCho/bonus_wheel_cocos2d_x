#!/bin/sh
set -e
if test "$CONFIGURATION" = "Debug"; then :
  cd /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios
  make -f /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "Release"; then :
  cd /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios
  make -f /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "MinSizeRel"; then :
  cd /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios
  make -f /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios/CMakeScripts/ReRunCMake.make
fi
if test "$CONFIGURATION" = "RelWithDebInfo"; then :
  cd /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios
  make -f /Users/juncho/Bonus_Wheel_Cocos2d-x/BonusWheel/proj.ios_mac/ios/CMakeScripts/ReRunCMake.make
fi

